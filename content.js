// Put a new div element into the DOM. It will be our overlay containing the word definitions.
var overlayElement = document.createElement('div');
overlayElement.setAttribute('class', 'overlay');
document.body.appendChild(overlayElement);

// Move the overlay to where the mouse is and show a message that the word is being looked up.
function moveOverlay(mouseX, mouseY, selectedText)
{
	var dx=Math.max(document.documentElement.scrollLeft,document.body.scrollLeft);
	var dy=Math.max(document.documentElement.scrollTop,document.body.scrollTop);
	overlayElement.style.top = (dy + mouseY) + 'px';
	overlayElement.style.left = (dx + mouseX) + 'px';
	overlayElement.style.visibility = 'visible';
	overlayElement.innerHTML = 'Looking up "' + selectedText + '"...';
}

// Show the overlay with some html (eg, the word definitions)
function showOverlay(html)
{
	overlayElement.innerHTML = html;
	overlayElement.style.visibility = 'visible';
}

// This is called when the http request has completed so we can show the result.
function httpReady()
{
	var parser = new DOMParser();
	var doc = parser.parseFromString(this.responseText, "text/html");
	var div = doc.querySelector('div.content');
	showOverlay(div.innerHTML);
}

// For now we only support words where every letter is Bangla or space or hyphen.
function isBangla(text)
{
	var i = text.length;
	while (i--)
	{
		var c = text[i];
		if (((c < '\u0980') || (c > '\u09ff')) && (c != '-') && (c != ' '))
			return false;
	}
	return true;
}

// Listen for mouse up events.
document.addEventListener('mouseup', function (e)
{
	var selectedText = window.getSelection().toString();
	// Only do something if some text is selected.
	if ((selectedText.length > 0) && (isBangla(selectedText)))
	{
		// Move the overlay to the mouse position.
		moveOverlay(e.clientX, e.clientY, selectedText)
		// Issue an http request for the word definitions.
		var url = 'http://www.banglatangla.com/master_search.php?word=' + selectedText;
		var req = new XMLHttpRequest();
		req.open("GET", url, true);
		req.onload = httpReady;
		req.send(null);
	}
}, false);

// When the mouse is released hide the overlay.
document.addEventListener('mousedown', function (e)
{
	overlayElement.style.visibility = 'hidden';
}, false);


